﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo02_Factoria
{
    // Una clase estática no puede tener instancias, sólo sirve para albergar
    // o agrupar métodos estáticos
    static class ClaseEstaticaFactoria
    {
        static List<string> lista = new List<string>();
        public static Producto Crear(int id)
        {
            string nombre;
            switch (id)
            {
                case 1: nombre = "Uno"; break;
                case 2: nombre = "Dos"; break;
                case 3: nombre = "Tres"; break;
                case 4: nombre = "Cuatro"; break;
                default: nombre = "Otro a saber"; break;
            }
            return new Producto(id, nombre);
        }
        public static Producto Crear(int id, string nombre)
        {
            return new Producto(id, nombre);
        }
        public static List<Producto> CrearLista(int b)
        {
            return CrearLista(1, b);
        }
        public static List<Producto> CrearLista(int a, int b)
        {
            List<Producto> l = new List<Producto>();
            for (int i = a; i <= b; i++)
            {
                l.Add(Crear(i));
            }
            return l;
        }
        public static List<Producto> CrearLista(int[] ids)
        {
            List<Producto> l = new List<Producto>();
            for (int i = 0; i < ids.Length; i++)
            {
                l.Add(Crear(ids[i]));
            }
            return l;
        }

    }
}
