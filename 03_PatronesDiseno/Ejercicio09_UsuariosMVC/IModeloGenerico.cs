﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    interface IModeloGenerico<Tipo>
    {
        Tipo Crear(Tipo tipo);
        Tipo Modificar(Tipo paramBuscado, Tipo paramNuevo);
        IList<Tipo> LeerTodos();
        Tipo LeerUno(Tipo param);
        bool Eliminar(Tipo param);
    }
}
