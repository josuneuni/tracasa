using NUnit.Framework;
using Ejemplo01_Singleton;
using System.Collections.Generic;
using System.Reflection;
using System;

namespace Ejemplo10_PruebasPatrones
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
            Assert.AreNotEqual(GestorTextos.Instancia, null);
        }

        [Test]
        public void Test1()
        {
            GestorTextos gt = GestorTextos.Instancia;
            gt.Nuevo("AAAAA");
            gt.Nuevo("BBB");
            gt.Nuevo("CCCC");
            gt.Mostrar();

            FieldInfo[] camposTextos = typeof(GestorTextos).GetFields(BindingFlags.NonPublic
            | BindingFlags.Instance);
            List<string> textos = null;
            foreach (FieldInfo field in camposTextos)
            {
                if (field.Name == "textos" )
                {
                    textos = (List<string>) field.GetValue(gt);
                    Console.WriteLine(textos.ToString());
                }
            }
            Assert.True(textos.Contains("BBB"), "Vaya, pues fallo y no hay BBB");
            Assert.Contains("CCCC", textos, "Vaya, pues fallo y no hay CCCC");
        }
    }
}