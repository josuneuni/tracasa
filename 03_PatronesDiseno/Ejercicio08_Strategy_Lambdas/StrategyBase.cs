﻿using System;
// No podemos usar nada de Ejercicio08_Strategy_Lambdas
// Ni de Program, ni de Main()
namespace OtroNameSpace
{
    delegate void VoidFunc();
    // Ya no se puede heredar de esta clase,  sino sobreescribir las funcionalidades
    // mediante delegados (variables de función, funciones estáticas y/o lambdas
    class StrategyObject
    {
        // private string nombre;

        public Func<string> GetNombre;
        public StrategyObject() : this("")
        {            
        }
        public StrategyObject(string mensaje) {
            this.GetNombre = () => {
                return mensaje + " " + GetType() + ".Execute()";
            };
            // Nombre = mensaje + " " + GetType() + ".Execute()";
            MostrarNombre = () =>
            {
                Console.WriteLine(this.GetNombre());
            };
        }

        public void RepetirChar(char caracter, int veces)
        {
            for (int i = 0; i < veces; i++)
            {
                Console.Write(caracter);
            }
            Console.Write("\n");
        }
        VoidFunc execute;

        // Este método tiene que ser otro delegado. VoidFunc = Action
        Action mostrarNombre;

        string type;

        internal VoidFunc Execute { get => execute; set => execute = value; }
        public Action MostrarNombre { get => mostrarNombre; set => mostrarNombre = value; }
        public string Type { get => type; set => type = value; }

        new string GetType()
        {
            return  Type;
        }
    }

    // Contexto ya no tiene sentido
    /*class Context
    {
        StrategyBase strategy;

        // Constructor
        public Context(StrategyBase strategy)
        {
            this.strategy = strategy;
        }

        public void Execute()
        {
            strategy.Execute();
        }
    }*/
}
