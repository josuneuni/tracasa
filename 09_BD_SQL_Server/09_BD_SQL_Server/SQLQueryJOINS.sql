﻿/*
SELECT C.* 
FROM Coche as C, Usuario as U
WHERE C.Id = U.idCoche AND U.edad > 50

SELECT SUM(UP.cantidad)
FROM Producto as P, Usuario as U, Usuario_Producto as UP
WHERE P.Id = UP.idProducto AND U.Id=UP.idUsuario AND U.edad <=20

SELECT U.Id,U.email,C.Marca,C.Modelo 
FROM Usuario as U INNER JOIN Coche as C
ON U.idCoche=C.Id

SELECT U.Id,U.email,C.Marca,C.Modelo 
FROM Usuario as U RIGHT JOIN Coche as C
ON U.idCoche=C.Id
where U.id is null

SELECT SUM(UP.cantidad)
FROM (Producto as P INNER JOIN Usuario_Producto as UP ON P.Id = UP.idProducto )INNER JOIN Usuario as U ON U.Id=UP.idUsuario
WHERE U.edad <=20
*/


/*
Por cada usuario, nombre de usuario y nombre de producto,
2 - El precio del producto de sus compras
3 - Cuanto se ha gastado en total en cada producto
4 - Cuantas copias del producto ha comprado
*/
/*
--PRECIO CADA COMPRA
SELECT U.Id,U.nombre 'NOMBRE USUARIO',sum(P.precio*UP.cantidad) 'GASTADO'
FROM (Producto as P FULL OUTER JOIN Usuario_Producto as UP ON P.Id = UP.idProducto ) RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario
group by  U.Id,U.nombre
ORDER BY U.Id

--GASTADO EN CADA PRODUCTO
SELECT U.Id,U.nombre 'NOMBRE USUARIO',P.Id,P.nombre,SUM(P.precio*UP.cantidad) 'GASTADO'
FROM (Producto as P FULL OUTER JOIN Usuario_Producto as UP ON P.Id = UP.idProducto ) RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario
group by  U.Id,U.nombre,P.Id,P.nombre
ORDER BY U.Id,P.Id

--CANTIDAD CADA USUARIO POR PRODUCTO
SELECT U.Id,U.nombre 'NOMBRE USUARIO',P.Id,P.nombre,SUM(UP.cantidad)
FROM (Producto as P FULL OUTER JOIN Usuario_Producto as UP ON P.Id = UP.idProducto ) RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario
group by  U.Id,U.nombre,P.Id,P.nombre
ORDER BY U.Id,P.Id
*/

/*

Por otro lado:
5 - Todas las ventas (dinero) del 2020
6 - Todos los usuarios que compraron en el 2019
7 - Qué productos se han vendido mejor a lo largo del 2018, 2019 y 2020
8 - El coche cuyo propietario ha comprado más a lo largo de la historia
9 - La media de edad de los compradores de cada uno de los productos (melón, cuajada...)
10 - Qué productos han comprado los usuarios del nissan
*/

SELECT SUM(P.precio*UP.cantidad)'GASTADO'
FROM (Producto as P FULL OUTER JOIN Usuario_Producto as UP ON P.Id = UP.idProducto ) RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario
WHERE UP.fecha BETWEEN '2020-01-01' AND '2020-12-31'

SELECT U.id,U.nombre
FROM (Producto as P FULL OUTER JOIN Usuario_Producto as UP ON P.Id = UP.idProducto ) RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario
WHERE UP.fecha BETWEEN '2019-01-01' AND '2019-12-31'
/*
SELECT *
FROM (
	SELECT TOP 1 P.nombre, SUM(UP.CANTIDAD)'CANTIDAD VENDIDA 2018'
	FROM (Producto as P FULL OUTER JOIN Usuario_Producto as UP ON P.Id = UP.idProducto ) RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario
	WHERE UP.fecha BETWEEN '2018-01-01' AND '2018-12-31'
	GROUP BY P.nombre
	ORDER BY SUM(UP.CANTIDAD) DESC)AS _2018,
	 (
	SELECT TOP 1 P.nombre, SUM(UP.CANTIDAD)'CANTIDAD VENDIDA 2019'
	FROM (Producto as P FULL OUTER JOIN Usuario_Producto as UP ON P.Id = UP.idProducto ) RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario
	WHERE UP.fecha BETWEEN '2019-01-01' AND '2019-12-31'
	GROUP BY P.nombre
	ORDER BY SUM(UP.CANTIDAD) DESC)AS _2019,
	 (
	SELECT TOP 1 P.nombre, SUM(UP.CANTIDAD)'CANTIDAD VENDIDA 2020'
	FROM (Producto as P FULL OUTER JOIN Usuario_Producto as UP ON P.Id = UP.idProducto ) RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario
	WHERE UP.fecha BETWEEN '2020-01-01' AND '2020-12-31'
	GROUP BY P.nombre
	ORDER BY SUM(UP.CANTIDAD) DESC)AS _2020
*/


/*
SELECT P.nombre,UP.range, SUM(UP.CANTIDAD)'cantidadvendida'
FROM (Producto as P FULL OUTER JOIN (  select idProducto,idUsuario,cantidad, case 
	when fecha BETWEEN '2018-01-01' AND '2018-12-31' then '2018'
	when fecha BETWEEN '2019-01-01' AND '2019-12-31' then '2019'
	when fecha BETWEEN '2020-01-01' AND '2020-12-31' then '2020'
	else 'resto' end as range
	from Usuario_Producto)as UP ON P.Id = UP.idProducto ) RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario
GROUP BY P.nombre,UP.range

SELECT P.nombre, SUM(UP.CANTIDAD)'cantidadvendida'
FROM (Producto as P FULL OUTER JOIN (  select idProducto,idUsuario,cantidad, case 
	when fecha BETWEEN '2018-01-01' AND '2018-12-31' then '2018'
	when fecha BETWEEN '2019-01-01' AND '2019-12-31' then '2019'
	when fecha BETWEEN '2020-01-01' AND '2020-12-31' then '2020'
	else 'resto' end as range
	from Usuario_Producto)as UP ON P.Id = UP.idProducto ) RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario
GROUP BY P.nombre

*/




/*
SELECT TOP 1 C.Marca,C.Modelo
FROM ((Producto as P INNER JOIN Usuario_Producto as UP ON P.Id = UP.idProducto ) RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario) LEFT JOIN Coche as C on C.id=U.idCoche
GROUP BY U.Id,C.Marca,C.Modelo
ORDER BY SUM(P.precio*UP.cantidad) DESC


SELECT P.Id, P.nombre,AVG(EDAD) 'EDAD'
FROM (Producto as P INNER JOIN Usuario_Producto as UP ON P.Id = UP.idProducto ) RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario
GROUP BY P.Id,P.nombre

SELECT DISTINCT P.Id, P.nombre
FROM ((Producto as P INNER JOIN Usuario_Producto as UP ON P.Id = UP.idProducto ) 
RIGHT JOIN Usuario as U ON U.Id=UP.idUsuario) LEFT JOIN Coche as C on C.id=U.idCoche
WHERE C.Marca='nissan'
*/