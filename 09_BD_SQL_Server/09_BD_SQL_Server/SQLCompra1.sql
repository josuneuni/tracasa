﻿DECLARE @Count AS INT
DECLARE @Fecha AS DATETIME
DECLARE @Cantidad AS INT
DECLARE @Usuario AS INT
DECLARE @Producto AS INT

DECLARE @start DATE = '1990-01-01'
DECLARE @EnD DATE = '2021-09-30'

DELETE FROM Usuario_Producto;

SET @Count=0
WHILE @Count < 20
BEGIN
	
	
	set @Usuario=32+RAND(CHECKSUM(NEWID())) *10
	set @Usuario=32+RAND(CHECKSUM(NEWID())) *10
	set @Producto=1+RAND(CHECKSUM(NEWID())) *4
	set @Cantidad=1+RAND(CHECKSUM(NEWID())) *100
	SET @Fecha=DATEADD(DAY,ABS(CHECKSUM(NEWID())) % ( 1 + DATEDIFF(DAY,@start,@EnD)),@start)
	INSERT INTO Usuario_Producto(idUsuario,idProducto,fecha,cantidad)
		VALUES (@Usuario,@Producto,@Fecha,@Cantidad)
	PRINT ' COUNT '+ CAST(@Fecha AS VARCHAR)
	SET @Count=@Count+1
END;
PRINT ('Final bucle');