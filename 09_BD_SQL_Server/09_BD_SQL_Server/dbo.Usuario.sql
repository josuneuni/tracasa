﻿CREATE TABLE [dbo].[Usuario] (
    [Id]     INT           IDENTITY (1, 1) NOT NULL,
    [email]  VARCHAR (255) NOT NULL,
    [nombre] VARCHAR (50)  NOT NULL,
    [edad]   TINYINT       NOT NULL,
    [altura] FLOAT (53)    NOT NULL,
    [activo] BIT           NOT NULL,
    [coche]  VARCHAR (50)  NULL,
    [direccion] VARCHAR(200) NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC),
    UNIQUE NONCLUSTERED ([email] ASC)
);

