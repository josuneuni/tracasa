﻿--PAra evitar duplicados
--SELECT DISTINCT edad FROM [dbo].[Usuario]
--SELECT COUNT(DISTINCT edad ) FROM [dbo].[Usuario]

--SELECT * FROM [dbo].[Usuario] WHERE edad>30

--SELECT * FROM [dbo].[Usuario] WHERE activo='True' AND edad>25
--SELECT * FROM [dbo].[Usuario] WHERE not(activo='True' AND edad>25)

--SELECT * FROM [dbo].[Usuario] ORDER BY edad DESC ,activo ASC

--SELECT * FROM [dbo].[Usuario] WHERE coche is not null

--SELECT * FROM [dbo].[Usuario] WHERE coche is not null
/*
SELECT * FROM(
SELECT TOP 5 * FROM [dbo].[Usuario] WHERE coche is null ORDER BY EDAD DESC
)AS SUBCONSULTA ORDER BY NOMBRE

*/
/*
SELECT id FROM [dbo].[Usuario] WHERE activo='False'

SELECT COUNT( DISTINCT inactivos.id) inactivos,COUNT(DISTINCT activos.id) activos FROM(
SELECT * FROM [dbo].[Usuario] WHERE activo='False' 
)AS inactivos,(
SELECT * FROM [dbo].[Usuario] WHERE activo='True' 
)AS activos

SELECT * FROM(
SELECT TOP 3 * FROM [dbo].[Usuario]ORDER BY ALTURA ASC
)AS SUBCONSULTA ORDER BY EDAD

SELECT * FROM
[dbo].[Usuario] AS T1,[dbo].[Usuario] AS T2
WHERE T1.coche=T2.coche AND T1.Id<T2.Id

SELECT TOP 1 *
FROM [dbo].[Usuario]
WHERE coche is not null 
ORDER BY edad DESC
*/

/*
SELECT TOP 1 *
FROM [dbo].[Usuario]
WHERE coche is not null  AND activo=0
ORDER BY edad ASC



SELECT MIN(Edad) 
FROM [dbo].[Usuario]
WHERE coche is null AND activo=0


SELECT COUNT(*) AS cantidad,
	CASE WHEN Coche is not null then Coche ELSE 'Sin coche' END
FROM Usuario
GROUP BY Coche
ORDER BY cantidad

*/

SELECT avg(CASE WHEN Coche is not null then CAST(Edad AS FLOAT) END) AS 'Con coche',
		avg(CASE WHEN Coche is null then CAST(Edad AS FLOAT) END) AS 'Sin coche',
		avg(CASE WHEN activo=0 then CAST(Edad AS FLOAT) END) AS 'Inactivo',
		avg(CASE WHEN Activo=1 then CAST(Edad AS FLOAT) END) AS 'Activo'
	
FROM Usuario

SELECT U.nombre,U.email FROM Usuario as U where U.email LIKE '%@gmail.com%'

SELECT *
FROM Usuario
WHERE edad>40 
