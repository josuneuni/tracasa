﻿DELETE FROM Usuario 

SET IDENTITY_INSERT [dbo].[Usuario] OFF
INSERT INTO [dbo].[Usuario] ( [email], [nombre], [edad], [altura], [activo]) VALUES ( N'aa@gmail.com', N'popo', 21, 1.7, 1)
INSERT INTO [dbo].[Usuario] ( [email], [nombre], [edad], [altura], [activo]) VALUES ( N'bb@gmail.com', N'pepe', 45, 2, 0)
INSERT INTO [dbo].[Usuario] ( [email], [nombre], [edad], [altura], [activo]) VALUES ( N'pecpe@gmail.com', N'AAA', 76, 1.5, 1)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES ( N'dd@gmail.com', N'SDFGDF', 12, 1.6, 0)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES ( N'ff@gmail.com', N'JHGJ', 34, 1.7, 0)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES ( N'gg@gmail.com', N'EWRWE', 77, 2, 0)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES ( N'gsrgs@gmail.com', N'LKLJKL', 34, 1.63, 1)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES ( N'retser@gmail.com', N'OUIOGU', 30, 1.78, 0)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES ( N'dtyujdty@gmail.com', N'HJKLHJ', 21, 2.1, 0)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES ( N'dtyudty@gmail.com', N'TYUITY', 6, 1.49, 0)
SET IDENTITY_INSERT [dbo].[Usuario] OFF
