﻿CREATE TABLE [dbo].[Usuario_Producto]
(
    [idUsuario] INT NOT NULL, 
    [idProducto] INT NOT NULL, 
    [fecha] DATETIME NOT NULL,
	[cantidad] TINYINT NOT NULL, 
    CONSTRAINT [PK_Usuario_Producto] PRIMARY KEY ([idUsuario], [fecha], [idProducto]) ,
	FOREIGN KEY (idUsuario) REFERENCES Usuario(Id),
	FOREIGN KEY (idProducto) REFERENCES Producto(Id)
)
