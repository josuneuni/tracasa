﻿using System;

namespace Ejemplo_Paso_Valor_Ref
{
    class Program
    {
        static void Main(string[] args)
        {
            int variableEnt = 10;

            Console.WriteLine("Entero fuera y antes: " + variableEnt);
            RecibimosUnValor(variableEnt);
            Console.WriteLine("Entero fuera y despues: " + variableEnt);

            RecibimosUnaRef(ref variableEnt);
            Console.WriteLine("Entero fuera y despues: " + variableEnt);

            string texto = "Hola que patxa";
            texto.ToUpper();
            Console.WriteLine("Nuevo texto: " + texto);
            EnMayus(texto);
            Console.WriteLine("Nuevo texto: " + texto);
            ConvertirMayus(ref texto);
            Console.WriteLine("Nuevo texto: " + texto);
        }
        static string EnMayus(string cad)
        {
            cad = cad.ToUpper();
            return cad;
        }
        static void ConvertirMayus(ref string cad)
        {
            cad = cad.ToUpper();
        }
        static void RecibimosUnValor(int entero)
        {
            Console.WriteLine("Entero dentro y antes: " + entero);
            entero = entero * 2 + 1;
            Console.WriteLine("Entero dentro y despues: " + entero);
        }
        static void RecibimosUnaRef(ref int entero)
        {
            Console.WriteLine("Entero dentro y antes: " + entero);
            entero = entero * 2 + 1;
            Console.WriteLine("Entero dentro y despues: " + entero);
        }

    }
}
