﻿using System;

namespace Ejemplo12_TratamientoCadenas
{
    class Program
    {
        static void Main(string[] args)
        {
            string texto = "     En un lugar de La Mancha de cuyo nombre me quiero acordar, pero no me acuerdo.     ";

            Console.WriteLine("Original:" + texto);
            Console.WriteLine("Sin espacios:" + texto.Trim());
            Console.WriteLine("Mayus:" + texto.ToUpper());
            Console.WriteLine("Minus:" + texto.ToLower());
            Console.WriteLine("Un cacho:" + texto.Substring(20, 20));
            Console.WriteLine("Un cacho hasta el final:" + texto.Substring(20));
            Console.WriteLine("La Mancha?:" + texto.IndexOf("La Mancha"));
            Console.WriteLine("LA MANCHA?:" + texto.ToUpper().IndexOf("LA MANCHA"));
            Console.WriteLine("Pamplona?:" + texto.IndexOf("Pamplona"));

            string[] palabras = texto.Trim().Split(" ");
            Console.WriteLine("Palabra por palabra: ");
            for (int p = 0; p < palabras.Length; p++)
            {
                Console.WriteLine("Palabra " + p + ": " + palabras[p]);
            }
            Console.WriteLine("Por Pamplona:" 
                + texto.Replace("La Mancha", "Pamplona")
                       .Replace("no", "Sarriguren"));

            Console.WriteLine("\n\n");
            palabras = new string[3];
            for (int p = 0; p < palabras.Length; p++)
            {
                Console.WriteLine("Escribe pal " + p);
                palabras[p] = Console.ReadLine().Trim();
                string sus = palabras[p];
                // while (sus != palabras[p].Replace("  ", " "))

                while (sus.IndexOf("  ") >= 0)
                {
                    sus = palabras[p].Replace("  ", " ");
                    palabras[p] = sus;
                }
            }
            string resultado = string.Join(",", palabras);
            Console.WriteLine("Resultado: " + resultado);

        }
    }
}
// Ejercicio: Pedir al usuario 3 palabras,
// y juntarlas en una única variable de tipo texto, separandolas por comas
// y sin espacios, como mucho 1 espacio entre palabras. 
/* 
 * "palabra"
 * "otra         palabra"
 * "   tercera   palabra"
 * "cuarta        palabra"
 * RESULTADO:    "
 * palabra,otra palabra,tercera palabra,cuarta palabra"

*/

