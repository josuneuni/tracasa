﻿using System;

namespace Ejercicio10_6_Coordenadas
{
   struct Vector2 {
        public float x, y;

        public Vector2(float paramx, float paramy )
        {
            this.x = paramx;
            this.y = paramy;
        }
        public float Longitud()
        {
            return (float) Math.Sqrt(x * x + y * y);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Vector2 v = new Vector2(3, 5);

            Console.WriteLine("X e Y: " + v.x + ", y = " + v.y);
            Console.WriteLine("longitud: " + v.Longitud());
            /*switch (v)
            {
                case Vector2(3, 5):
                    break;
            }*/

            Console.WriteLine("Cuantos? ");
            int c = int.Parse(Console.ReadLine());
            int[] cu = new int[4];

            for (int i = 0; i < c; i++)
            {
                Console.WriteLine("X? ");
                int x = int.Parse(Console.ReadLine());
                Console.WriteLine("Y? ");
                int y = int.Parse(Console.ReadLine());

                cu[0] += (x >= 0 && y >= 0) ? 1 : 0;
                cu[1] += (x >= 0 && y < 0) ? 1 : 0;
                cu[2] += (x < 0 && y < 0) ? 1 : 0;
                cu[3] += (x < 0 && y >= 0) ? 1 : 0;
            }
            for (int i = 0; i < 4; i++)
                Console.WriteLine("C " + i + " = " + cu[i]);
        }
    }
}