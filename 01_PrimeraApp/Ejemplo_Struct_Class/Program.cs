﻿using System;

namespace Ejemplo_Struct_Class
{
    static class ClaseDeLaQueNoPuedeHaberObjeto
    {
        // Sólo podemos declarar variables y métodos estáticos
        static int normal;
        static void Met() { }
    }
    /*
 * Crear una estructura ProductoE indepe (NO anidada) con nombre y precio, su constructor y una funcion para mostrar sus datos
 * 
 * Crear una clase ProductoC indepe con nombre y precio, su constructor y una funcion para mostrar sus datos
 * 
 * Crear 4 funciones estáticas en Program:
 * -Una que reciba ProductoE y modifique su nombre y su precio
 * -Otra que reciba una referencia ProductoE y modifique su nombre y su precio
 * 
 * -Una que reciba ProductoC y modifique su nombre y su precio
 * -Otra que reciba una referencia ProductoC y modifique su nombre y su precio
 * 
 * Finalmente, comprobar en main el comportamiento para saber cuál modifica realmente la variable original
 */

    /*  1 - Las estructuras siempre se pasan por valor a menos que se indique aposta
     *  con ref que es por referencia
     *  2 - No pueden heredar  unas estructuras de otras
     *  3 - No pueden haber estructuras estáticas
     *  4 - Siempre siempre tienen un constructor por defecto
     */
    struct ProductE
    {
        public string nombre;
        public int precio;

        public ProductE(string nombre, int precio)
        {
            this.nombre = nombre;
            this.precio = precio;
        }

        public void MostrarDatos()
        {
            Console.WriteLine("Nombre del producto (estructura): " + nombre + ", con un precio de: " + precio);
        }
    }
 
    /*  1 - Las clases siempre se pasan por  referencia. Es redundante usar ref
  *  2 - Sí pueden heredar unas clases de otras
  *  3 - Sí pueden haber clases estáticas
  *  4 - Sólo tienen un constructor por defecto cuando no hemos 
  *       creado un contructor explícitamente con algún argumento
  */

    class ProductC
    {
        /* string es una clase especial que se comporta más como tipo primitivo */ 
        public string nombre;
        /* Los tipos primitivos en realidad son estructuras */
        public int precio;

        public ProductC()
        {
        }
        public ProductC(string nombre, int precio)
        {
            this.nombre = nombre;
            this.precio = precio;
        }

        public void MostrarDatos()
        {
            
            Console.WriteLine("Nombre del producto (clase): " + nombre + ", con un precio de: " + precio);
        }
    }

    static  class Program
    {
        static void Main(string[] args)
        {
            ProductE otroProdE = new ProductE();
            otroProdE.nombre = "Pues le damos un valor " + 3434.ToString();
            otroProdE.MostrarDatos();
            ProductC otroProdC = new ProductC();
            otroProdC.nombre = "Pues le damos un valor";
            otroProdC.MostrarDatos();

            ProductE EEE = new ProductE("LA LUNA", 100000);
            ProductC CCC = new ProductC("Iphone", 1000);

            Console.WriteLine("VALORES ANTES");
            EEE.MostrarDatos();
            CCC.MostrarDatos();
            Console.WriteLine("=========================");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");

            modifValorE(EEE);
            EEE.MostrarDatos();
            modifRefE(ref EEE);
            EEE.MostrarDatos();

            modifValorC(CCC);
            CCC.MostrarDatos();
            modifRefC(ref CCC);
            CCC.MostrarDatos();
        }

        static void modifValorE(ProductE producto)
        {
            producto.nombre = "MODIFICANDO POR VALOR. ESTRUCTURA";
            producto.precio = 10;
        }
        static void modifRefE(ref ProductE producto)
        {
            producto.nombre = "MODIFICANDO POR REFERENCIA. ESTRUCTURA";
            producto.precio = 20;
        }

        //==============================================================================

        static void modifValorC(ProductC producto)
        {
            producto.nombre = "MODIFICANDO POR VALOR. CLASE";
            producto.precio = 30;
        }
        static void modifRefC(ref ProductC producto)
        {
            producto.nombre = "MODIFICANDO POR REFERENCIA. CLASE";
            producto.precio = 40;
        }


        // los objetos de clase se modifican por referencia. Los objetos-estructura NO se modifican por referencia excepto si se indica que lo haga.
        // Estructuras NO heredan y NO pueden ser estáticos
    }
}
