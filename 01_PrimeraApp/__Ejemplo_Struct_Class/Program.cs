﻿using System;

namespace Ejemplo_Struct_Class
{
    /*
     Crear una estructura ProductoE independiente (NO anidada)
        con nombre y precio, y su contructor
        con una función para  mostrar sus datos
    
     Crear una clase ProductoC independiente (NO anidada)
        con nombre y precio, y su contructor
        con una función para  mostrar sus datos

     Crear 4 funciones estáticas en Program:
        - Una que reciba ProductoE y modifique su nombre y su precio
    static void ModificarProductoE(ProductoE p) {
        p.nombre = "Otro nom 23";
        p.precio = 445;
    }
        - Otra que reciba una ref ProductoE y modifique su nombre y su precio
      - Una que reciba ProductoC y modifique su nombre y su precio
        - Otra que reciba una ref ProductoC y modifique su nombre y su precio

    Por último, en Main(), comprobar el comportamiento para saber
    cual modifica realmente la variable original 
     */
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
