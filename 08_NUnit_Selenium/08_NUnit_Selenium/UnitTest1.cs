using NUnit.Framework;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;

namespace _08_NUnit_Selenium
{
    public class Tests
    {
        //objeto que se va a conectar con el navegador
        IWebDriver driver;
        private int seg;

        [OneTimeSetUp]

        public void InicializarClaseTest()
        {
            string fichFirefox = "../../../../FirefoxPortable/App/Firefox64/Firefox.exe";
            if (!File.Exists(fichFirefox))
            {
                string instalador = "../../../../FirefoxPortable_92.0_English.paf.exe";
                if (File.Exists(instalador))
                {
                    System.Diagnostics.Process.Start(instalador);
                }

            }
            if (File.Exists(fichFirefox))
            {
                //string rutaDriverFirefox = "geckodriver.exe";
                //FirefoxBinary binarioFirefox = new FirefoxBinary(fichFirefox);
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                firefoxOptions.BrowserExecutableLocation = fichFirefox;
                driver = new FirefoxDriver(firefoxOptions);
            }
        }

        [OneTimeTearDown]
        public void FinalizarClaseTest()
        {
            //driver.Close();
        }

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            driver.Navigate().GoToUrl("https://duckduckgo.com/");
            IWebElement textoBusqueda = driver.FindElement(By.Name("q"));
            textoBusqueda.SendKeys("SQL Tutorials w3school create table");
            IWebElement botonBusqueda = driver.FindElement(By.Id("search_button_homepage"));
            botonBusqueda.Click();
            //var enlaces = driver.FindElements(By.CssSelector(".result__a"));
            var enlaces = driver.FindElements(By.CssSelector("a[href*='https://www.w3schools.com'"));
            Actions actions = new Actions(driver);
            

            foreach (var enlace in enlaces)
            {
                if ((!enlace.Equals(enlaces[0]))&&enlace.Displayed)//No hace falta hacer el for pa esto, pero as� vemos que se puede hacer
                {
                        actions.MoveToElement(enlace);
                        actions.Perform();
                        enlace.Click();
                        break;
                    
                }
            }
            Actions actions2 = new Actions(driver);
            Assert.GreaterOrEqual(enlaces.Count, 3, "No se han encontrado suficientes enlaces");
            driver.FindElement(By.Id("accept-choices")).Click();
            var enlaceSQL= driver.FindElement(By.CssSelector("a[href='sql_datatypes.asp'"));
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", enlaceSQL);
            /*actions2.MoveToElement(enlaceSQL);
            actions2.Perform();*/
            Wait(3);
            enlaceSQL.Click();

            IWebElement tituloNumDT = driver.FindElement(By.XPath("//h3[text()='Numeric Data Types'][2]"));
            Assert.IsNotNull(tituloNumDT, "No se han encontado los tipos de datos num�ricos");
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", tituloNumDT);

            //IWebElement tabla = driver.FindElement(By.XPath("/html/body/div[7]/div[1]/div[1]/div[8]/table"));
            var filas = driver.FindElements(By.XPath("/html/body/div[7]/div[1]/div[1]/div[8]/table/tbody/tr"));
            foreach (var fila in filas)
            {
                if(!fila.Equals(filas[0]))
                Console.WriteLine(fila.Text);
            }
        }

        [Test]
        public void tabla()
        {
            driver.Navigate().GoToUrl("https://www.w3schools.com/sql/sql_datatypes.asp");
            IWebElement tituloNumDT = driver.FindElement(By.XPath("//h3[text()='Numeric Data Types'][2]"));
            Assert.IsNotNull(tituloNumDT, "No se han encontado los tipos de datos num�ricos");
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", tituloNumDT);

            //Contiene la columna de datatype 
            var filas = driver.FindElements(By.XPath("/html/body/div[7]/div[1]/div[1]/div[8]/table/tbody/tr/td[1]"));

            //Contiene la columna de storage
            var filas2 = driver.FindElements(By.XPath("/html/body/div[7]/div[1]/div[1]/div[8]/table/tbody/tr/td[3]"));

            //Contiene la columna de encabezado
            var encabezado = driver.FindElements(By.XPath("/html/body/div[7]/div[1]/div[1]/div[8]/table/tbody/tr[1]/th"));

            Assert.AreEqual(filas.Count, filas2.Count, "No coinciden los elementos");
            Assert.GreaterOrEqual(filas.Count, 5, "No cuadran las columnas");
            Assert.AreEqual(filas[filas.Count-1].Text, "real", "La �ltima fila debe ser real");

            Console.WriteLine(encabezado[0].Text + "--------" + encabezado[2].Text);
            int i = 0;
            foreach (var fila in filas)
            {
                Console.WriteLine(fila.Text + "--------" + filas2[i].Text );
                i++;
            }

        }

        public void Wait(int ms, int timeOut = 60)
        {
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 1, timeOut));
            var delay = new TimeSpan(0, 0, 0, seg);
            var timeInic = DateTime.Now;
            wait.Until(driver => (DateTime.Now - timeInic) > delay);//mejor hacer until exista el elemento

        }
    }
}