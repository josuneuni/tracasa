﻿using System;
using System.Threading;
using NUnit.Framework;


namespace TestProject1
{
    public class Class1
    {
        [Test]

        public void ProbandoHilos()
        {
            Console.WriteLine("Arrancando ProbandoHilos");
            Thread hilo1 = new Thread(FuncionHilo1);
            Thread hilo2 = new Thread(FuncionHilo2);
            hilo1.Start();
            hilo2.Start();
            Console.WriteLine("Terminando 1");
            Thread.Sleep(500);
            Console.WriteLine("Terminando probandohilos1");
        }
        static int i = 0;
        public static void FuncionHilo1()
        {
            Console.WriteLine("Arrancando hilo 1");
            for ( i = 0; i < 100000000 ; i++);
            Console.WriteLine("Terminando hilo 1");
        }

        public static void FuncionHilo2()
        {
            Console.WriteLine("Arrancando hilo 2");
            for ( i = 10000000; i >0 ; i--) ;
            Console.WriteLine("Terminando hilo 2");
        }
    }

}
