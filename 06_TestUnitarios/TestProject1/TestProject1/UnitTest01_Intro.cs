using NUnit.Framework;
using System;

namespace TestProject1
{
    public class Tests
    {
        string texto;

        //Este es el m�todo de inicializaci�n porque lleva el atrubuto C# [SetUp]
        // Los atributos son "Decoradores", caracter�sticas que a�adimos a clases, propiedades y m�todos
        //para agregar cierta funcionalidad
        [SetUp]// se ejecuta una vez antes de cada m�todo
        public void Inicializacion()
        {
            texto = "Texto inicial";
        }

        [Test]
        public void TestPrimero()
        {
            // Assert = asegurar
            Assert.Pass();
        }

        [Test]
        public void TestSegundo()
        {
            // Assert = asegurar
            Assert.AreEqual(1+3,2+2,"no son iguales");
            Assert.AreNotEqual(1 + 3, 2 + 0,"son iguales");
            Assert.IsNotNull(this.texto, "El texto es nulo");
            texto = "aaaaaaaaaaaaaaaa";
        }

        public static void DelegadoCualquieraOk()
        {
            Console.WriteLine("Delegado Cualquiera Ok");
        }

        public static void DelegadoCualquieraMal()
        {
            throw new NotImplementedException("Delegado Cualquiera Mal");
        }


        [Test]
        public void TestTercero()
        {
            Console.WriteLine("Antes");
            // Assert = asegurar
            Assert.Contains('i', texto.ToCharArray(), "El array de char texto no tiene 'i'");
            Assert.IsTrue(texto.Equals("Texto inicial")," El texto no es igual");
            Assert.IsNotNull(this.texto,"El texto es nulo");
            
            TestDelegate delegateOk= DelegadoCualquieraOk;
            Assert.DoesNotThrow(delegateOk,"Delegado cualquiera Ok fall�");

            Assert.Throws<NotImplementedException>(DelegadoCualquieraMal, "Delegado cualquiera Ok casc�");

            Console.WriteLine("Despues");

        }

        //isNaN Inconclusive
        

        [Test(Author ="Josune S",Description ="Prueba de Assert isNaN")]
        public void TestIsNan_Correcto_Incorrecto() 
        {
            double zero = 0;
            Assert.IsNaN(zero / zero, "Es un n�mero");
            Assert.IsNaN(Math.Sqrt(-1), "Es un n�mero");
            Assert.IsNaN(Math.Sqrt(1), "1 es un n�mero");//Falla porque es un n�mero
            Assert.IsNaN(80 + 40, "120 es un numero");//Falla porque es un n�mero
            Assert.Inconclusive();//Si ha habido un error previo, no se ejecuta

        }


        //Inconclusive
        //Inconclusive test is a test for which you cannot determine the result.
        //For example, if you have a test that uses some kind of an external resource (Internet connection, for example). 

        [Test(Author = "Josune S", Description = "Prueba de Assert Inconclusive con mensaje")]
        public void TestIncloncusive_ConMensaje()
        {
            bool condicion=true;
            if(condicion)
                Assert.Inconclusive("Test inconclusive");
        }
    }
}