﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo01_Encapsulacion
{
    public interface IEditableConsola
    {
        void PedirDatos();
        void MostrarDatos();
    }
}
