﻿using Ejemplo01_Encapsulacion;
using Ejemplo02_Herencia;
using Ejercicio01_Encapsulacion;
using System;
using System.Collections.Generic;

namespace Ejemplo03_Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            // Por lo general no es aconsejable crear un array de objetos
            // OJO: Esto es sólo a nivel educativo
            // Lo normal es declarar un array de clases, interfaces ó  clases abstractas
            object[] popurri = new object[3];
            popurri[0] = new Coche("Fiat", "Punto", 9000);
            popurri[1] = new CocheElectrico("Fiat", "Punto", 9000);
            popurri[2] = new Usuario("Fulanito", 50, 2);

            foreach (object objQueSea in popurri)
            {
                Console.WriteLine(objQueSea.ToString());
            }
            Array.Resize<object>(ref popurri, 20);
            ((Coche)popurri[0]).SetNombre("FIAT - PUNTO 4.5");
            Coche fiatPunto = (Coche) popurri[0];

            Console.WriteLine(fiatPunto.GetNombre().ToUpper().Trim());

            // El polimorfismo se puede usar con interfaces
            INombrable fiatNombrable = /*(INombrable)*/ fiatPunto;
            fiatNombrable.Nombre = "Fiat - Punto Version 1034";
            Console.WriteLine(fiatNombrable.Nombre.Trim());

            INombrable cen = (CocheElectrico)popurri[1];
            Console.WriteLine(cen.GetType().Name + ":" + cen.GetNombre());
            /*
            char[] caract = {'A', 'B', 'C'};
            string letra = (" " + caract[1]).ToUpper();
            char caracterSolito = letra.ToCharArray()[0];
            char caracterSolito1 = letra.ToCharArray()[1];
            */
            EjemploLista();
        }
        static void EjemploLista()
        {
            List<string> textos = new List<string>(3);
            // Por defecto, se crean 10 ó 20 elementos, pero esto nos da igual
            // lo hace de manera interna. Podemos especificar el tamño inicial (3)
            textos.Add("Primer texto");
            textos.Add("2do texto");
            textos.Add("Tercer texto");
            textos.Add("Otro texto");   // Internamente va usando el array
            for (int i = 0; i < 5; i++)
                textos.Add("Texto " + i + " º ");
            // LLegará un punto que internamente tenga que ampliar dinámicamente

            textos.RemoveAt(2); // Y al eliminar, tendrá que internamente reestructurar
            textos.Remove("Otro texto");    // Estas operaciones cuestan trabajo
            foreach (string x in textos)
                Console.WriteLine("Elem: " + x);

            List<Coche> coches = new List<Coche>();
            coches.Add(new Coche("Toyota", "Cupra", 50000));
            coches.Add(new CocheElectrico("Tesla", "F21", 40000));
            coches[0].MostrarDatos();

            IList<Coche> icoches = coches;
            icoches.Add(new Coche());
            IList<Usuario> listaUsu = new List<Usuario>();
            listaUsu.Add(new Usuario("Usuario de Lista 1", 0, 0));
            listaUsu.Add(new Usuario("Usuario de Lista 2", 0, 0));

            IList<Usuario> arrayUsu = new Usuario[10];
            arrayUsu[0] = new Usuario("Usuario de Array 1", 0, 0);
            arrayUsu[1] = new Usuario("Usuario de Array 2", 0, 0);
            arrayUsu[5] = new Usuario("Usuario de Lista Quinto", 0, 0);

            MostrarColeccion(listaUsu);
            MostrarColeccion(arrayUsu);
        }
        static void MostrarColeccion(ICollection<Usuario> icoleccion)
        {
            Console.WriteLine("Coleccion usuarios " + icoleccion.GetType());
            foreach (Usuario usu in icoleccion)
            {
                // Usuario usu = icoleccion[i];   No es un array
                /*if (usu != null)
                    usu.MostrarDatos(); */
                // Esta es una abreviatura de la anterior: El ? antes de un método pregunta si es null
                if (usu != null)
                    usu?.MostrarDatos();

                /*try
                {
                } catch (Exception ex)
                {
                    Console.WriteLine("Hay elemento vacio: " + ex.Message);
                }*/
            }
        }
    }
}